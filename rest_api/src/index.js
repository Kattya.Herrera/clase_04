const express = require('express')
const bodyParser = require('body-parser')
const { request, response } = require('express')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: true
}))

app.get('/', function(request,response){
    //request me trae los datos de las llamadas
    //response envia los datos hacia quien llamo
    response.send('Hola MUNDO desde API REST')
})

app.get('/:num1/:num2', (request, response)=>{
/*    const value1 = Number(request.params.num1)
    const value2 = Number(request.params.num2)
    const sum = value1 + value2*/

    response.json({result: sumValues(request.params.num1,request.params.num2)})
}) // -->/5/3

app.post('/',(request, response) =>{
    if(request.body.num1 && request.body.num2){
        response.status(200).json({result: multiply(request.body.num1,request.body.num2)})
    }
    else{
        response.status(404).json({error:"Something is missing!"})
    }
    })

app.listen(3001, function(){
    console.log('Server is running in port 3001')
}) 

function sumValues(num1,num2){
    return Number(num1) + Number(num2)
}

function multiply(num1,num2){
    return Number(num1) * Number(num2)
}